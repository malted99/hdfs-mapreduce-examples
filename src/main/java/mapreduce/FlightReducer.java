package mapreduce;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

// Eingabe-Key, Eingabe-Wert, Ausgabe-Key, Ausgabe-Wert
public class FlightReducer extends Reducer<IntWritable, IntWritable, IntWritable, FloatWritable> {
	
	@Override
	protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {

		float sum = 0;
		float count = 0;
		for (IntWritable val : values) {
			sum +=val.get();
			count +=1;
		}
	
		float result = sum / count;
		
		context.write(key, new FloatWritable(result));
		
	}
}
