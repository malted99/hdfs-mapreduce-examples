package mapreduce;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

// Eingabe-Key, Eingabe-Wert, Ausgabe-Key, Ausgabe-Wert
public class FlightMapper extends Mapper<Text,Text,IntWritable,IntWritable> {
	
	private IntWritable year_int = null;
	private IntWritable passengers_int = null;

	public void map(Text key, Text value, Context context) throws IOException,
			InterruptedException {
		
		// Auslesen aus einem String wie "1090112052010120"
		if (key.toString().length() == 16) {
			String year_str = key.toString().substring(9,13);
			String passengers_str = key.toString().substring(13,16);
			
			//System.out.println("Jahr: " + year_str);
			//System.out.println("Passagiere: "+passengers_str);
			
			year_int = new IntWritable(Integer.parseInt(year_str));
			passengers_int = new IntWritable(Integer.parseInt(passengers_str));
			
			// Sammeln der Ergebnisse
			context.write(year_int, passengers_int);
		} else {
			System.out.println("Falsche Eingabedaten");
		}
	}
}
