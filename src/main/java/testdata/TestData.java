package testdata;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestData {

	public static void main(String[] args) {
		
		int data_counter = 20000;
		
		try {
			Random r = new Random();
			PrintWriter out = new PrintWriter("mr_testdata.txt");
			
			int flight_id;
			Date test_date = new Date();
			Calendar c = Calendar.getInstance();
			DateFormat dfmt = new SimpleDateFormat("ddMMyyyy");
			int passengers;
			
			for(int i=0; i<data_counter; i++) {
				
				// Flugnummer
				flight_id = 10000 + r.nextInt(1000);
				
				// Datum des Flugs
				c.set(Calendar.YEAR,         2000 + r.nextInt(15));
				c.set(Calendar.MONTH,        1    + r.nextInt(11));
				c.set(Calendar.DAY_OF_MONTH, 1    + r.nextInt(30));
				test_date = c.getTime();
				
				// Passagierzahl
				passengers = 100 + r.nextInt(99);
				
				// Schreibe Datei
				out.print(flight_id);
				out.print(dfmt.format(test_date));
				out.print(passengers);
				out.print(System.getProperty("line.separator"));
			}
			
			out.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
