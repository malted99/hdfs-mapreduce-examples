package writefiles;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class WriteFileToHDFS {

	public static void main(String[] args) throws Exception {
		 
		Scanner sc = new Scanner(System.in);
		
		//Source file in the local file system
		//String localSrc = args[0];
		
		System.out.println("copy | list | write | read | delete");
		
		String choice = sc.next();
		try {
			if(choice.equals("list")) {
				System.out.println("Path:");
				String path = sc.next();
				//if(path.equals("")) path = "/";
				showFiles(path);
			}
			if(choice.equals("copy")) {
				copyFileOrFolder(sc);	
			}
			if(choice.equals("write")) {
				writeFile(sc);
			}
			if(choice.equals("read")) {
				readFile(sc);
			}
			if(choice.equals("delete")) {
				deletePath(sc);
			}
		} catch (Exception e) 
		{
			System.out.println("Exception caught! Check your input");
			System.out.println(e);
		}

		main(args);
	}
	
	private static void deletePath(Scanner sc) throws Exception
	{
		System.out.println("Path to File or Folder:");
		String filePath = sc.next();
		
		Path path = new Path(filePath);
		
		Configuration configuration = new Configuration();
		configuration.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
		configuration.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
		
		FileSystem hdfsFileSystem = FileSystem.get(new URI("hdfs://quickstart.cloudera:8020"),configuration);	
		
		if(hdfsFileSystem.exists(path)) {
			System.out.println("Delete? true | false");
			if(sc.nextBoolean() == true) {
				hdfsFileSystem.delete(path, true);
			}
		} else {
			System.out.println("No File or Folder found");
		}
		
	}

	private static void readFile(Scanner sc) throws Exception 
	{
		System.out.println("Path+File:");
		String filePath = sc.next();
		
		Path path = new Path(filePath);
		
		Configuration configuration = new Configuration();
		configuration.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
		configuration.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
		
		FileSystem hdfsFileSystem = FileSystem.get(new URI("hdfs://quickstart.cloudera:8020"),configuration);
		
        BufferedReader br=new BufferedReader(new InputStreamReader(hdfsFileSystem.open(path)));
        String line;
        line=br.readLine();
        System.out.println("File content:");
        while (line != null){
                System.out.println(line);
                line=br.readLine();
        }
        System.out.println("");
	
	}

	private static void writeFile(Scanner sc) throws Exception 
	{
		System.out.println("Insert text to write:");
		String textToWrite = sc.next();
		InputStream in = new BufferedInputStream(new ByteArrayInputStream(textToWrite.getBytes()));
		System.out.println("HDFS Path+File:");
		String filePath = sc.next();
		Path toHdfs = new Path(filePath);
		
		Configuration configuration = new Configuration();
		configuration.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
		configuration.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
		//FileSystem hdfsFileSystem = FileSystem.get(configuration);
		FileSystem hdfsFileSystem = FileSystem.get(new URI("hdfs://quickstart.cloudera:8020"),configuration);
		
		if(hdfsFileSystem.exists(toHdfs)) {
			System.out.println("Target exists! Delete and recreate? true | false");
			if(sc.nextBoolean() == true) {
				hdfsFileSystem.delete(toHdfs, true);
			}
		}
		
		FSDataOutputStream out = hdfsFileSystem.create(toHdfs);
		
		IOUtils.copyBytes(in, out, configuration);
		
	}

	public static void showFiles(String path2) throws Exception 
	{	
		Configuration configuration = new Configuration();
		configuration.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
		configuration.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
		//FileSystem hdfsFileSystem = FileSystem.get(configuration);
		FileSystem hdfsFileSystem = FileSystem.get(new URI("hdfs://quickstart.cloudera:8020"),configuration);
		//path2 = "/user/cloudera/";
		Path path = new Path(path2);
		
		FileStatus[] files = hdfsFileSystem.listStatus(path);
		for(FileStatus file : files) {
			System.out.println(file.getPath().getName());
		}
	}
	
	public static void copyFileOrFolder(Scanner sc) throws Exception {
		System.out.println("Source file:");
		String localSrc = sc.next();
		
		//Destination file in HDFS
		//String dst = args[1];
		
		System.out.println("Destination file:");
		String dst = sc.next();
		Path dstPath = new Path(dst); 
		
		
		File sourceFile = new File(localSrc);
		if(sourceFile.isDirectory()) {
			
		}
		
		//Input stream for the file in local file system to be written to HDFS
		InputStream in = new BufferedInputStream(new FileInputStream(localSrc));
		 
		//Get configuration of Hadoop system
		Configuration configuration = new Configuration();
		configuration.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
		configuration.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
	
		System.out.println("Connecting to -- "+configuration.get("fs.defaultFS"));
		 
		//Destination file in HDFS
		//FileSystem fs = FileSystem.get(URI.create(dst), configuration);
		FileSystem hdfsFileSystem = FileSystem.get(new URI("hdfs://quickstart.cloudera:8020"),configuration);
		
		if(hdfsFileSystem.exists(dstPath)) {
			System.out.println("Target exists, delete first.");
		} else {
			
		}
		
		OutputStream out = hdfsFileSystem.create(dstPath);
		 
		//Copy file from local to HDFS
		IOUtils.copyBytes(in, out, 4096, true);
		 
		System.out.println(dst + " copied to HDFS");
	}
	
}
